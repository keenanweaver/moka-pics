#!/usr/bin/env bash
chosen="false"
until [ "$chosen" = "true" ]
do
    # Choose moka
    moka=$(cat /home/keenan/moka | shuf -n 1)
    # Trim moka
    mokacut=$(echo $moka | cut -c 1050-1060)
    # Filter moka
    if grep -q "$mokacut" /home/keenan/moka-filter; then
        echo -n ""
    else
        # Crown moka
        echo $mokacut | tee -a /home/keenan/moka-filter
        echo $moka | tee /home/keenan/moka-picked
        awk 'NR==1{print; print "<br /><b>Moka of the Day!</b><br />"} NR!=1' /home/keenan/moka-picked > /home/keenan/welcome.txt
        chosen="true"
    fi
done